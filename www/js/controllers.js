angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout , $rootScope) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

})

.controller('PlaylistsCtrl', function($scope,$http,$rootScope,$timeout,$ionicScrollDelegate,$ionicLoading) 
{
	$scope.$on('$ionicView.enter', function(e) 
	{
		//$timeout(function() 
		//{
			
		//	$scope.CustomersNames = $rootScope.Customers;
			$scope.CustomerSearch = "";
			$scope.Host = $rootScope.Host				
			$rootScope.$watch('Customers', function() 
			{
    		    $scope.Customers = $rootScope.Customers;
				/*$scope.CustomersNames = [{
									"AMID": "IL600026090",
									"AccountName": ".CROW TECHNOLOGIES 1977 LTD ",
									"Region": "EMEA",
									"Subregion": "CEE",
									"AccountType": "images/customers/ABB.png",
									"Roster": [],
									"SupportCases": [],
									"SupportIncidents": [],
									"CustomerOrders": []
								}]*/
								//console.log($scope.CustomersNames)
			});
			
			$scope.UpdateCustomers = function()
			{
				if ($scope.CustomerSearch)
				{
					console.log("Update : " ,$scope.CustomerSearch )
					
					$ionicLoading.show({
						  template: 'Loading customers...',
						  //noBackdrop : true,
						  duration : 10000				  
					})				
					

					$scope.searchheaders = 
					{
						//'Authorization': 'Basic ' + _authdata,
						'Cookie:': $rootScope.cookie,
						'Content-Type': 'application/json; charset=utf-8'               
					}

				
					$http.get('https://admast01web.saas.hpe.com/main_project/api/v1/viewer/customers/'+$scope.CustomerSearch+'/search',$scope.searchheaders)
					.success(function (response) 
					{
						 $ionicLoading.hide();
						 var response  = JSON.parse(response);
						 $rootScope.Customers = response;	
					});					
				}
				else
				{
					$rootScope.loadCustomers();
				}

			}
			
			$scope.blurSearch = function()
			{
				$ionicScrollDelegate.scrollTop();
			}
			
			$scope.enterPress = function(keyEvent)
			{
				if (keyEvent.which === 13)
				{
					$scope.UpdateCustomers();
				}
			}			
		
		
			$scope.$watch('CustomerSearch', function() 
			{
				if ($scope.CustomerSearch)
				{
					$rootScope.SearchSupplierFlag = 1;
				}
        	});
			
			
			$rootScope.$watch('AndroidbackButton', function() 
			{
				if ($rootScope.AndroidbackButton == 1)
				{
					$scope.CustomerSearch = '';
				}
        	});

			
			

			
		//}, 0);
	});
})

.controller('SingleCtrl', function($scope, $stateParams,$rootScope,$timeout,$http,$ionicLoading,$ionicScrollDelegate) 
{
	//$scope.$on('$ionicView.enter', function(e) 
	//{
		//$timeout(function() 
		//{
			$scope.Id = $stateParams.playlistId;
			$scope.Customer = $rootScope.Customers;
			$scope.CustomerName = $scope.Customer.AccountName
			$scope.Contacts = "";
			$scope.SupportCases = [];
			$scope.isLoad = true;
			$scope.navTitle='<p class="HeaderTitle"></p>'
			$scope.AMID = $stateParams.playlistId;
			$scope.Host = $rootScope.Host;
			$scope.CustomerInfo = '';
			$scope.ordersLenth = 0;
			$scope.customrerJson = 
			{
				"name" : ""
			}
			//alert ($scope.Id);
			
			$scope.TabSelected = 1;
			
			$ionicLoading.show({
				  template: 'Loading customer details ...'
			})
			
			console.log("Start")
			//$http.get('https://admast01web.saas.hpe.com/main_project/api/v1/viewer/customers/US055169452')
			/*$http.get('http://www.tapper.co.il/hp.php')
			.success(function (response) 
			{
				console.log("Load")
				 $scope.SupportCases = response.SupportCases;
				 $scope.SupportIncidents = response.SupportIncidents;
				 $ionicLoading.hide();
				 $scope.isLoad = false;
			});*/
			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			console.log('https://admast01web.saas.hpe.com/main_project/api/v1/viewer/customers/'+$scope.AMID)
			

			$scope.headers = 
			{
				//'Authorization': 'Basic ' + _authdata,
				'Cookie:': $rootScope.cookie,
				'Content-Type': 'application/json; charset=utf-8'               
			}

							
			$http.get('https://admast01web.saas.hpe.com/main_project/api/v1/viewer/customers/'+$scope.AMID,$scope.headers).success(function(response)
			//$http.get('https://admast01web.saas.hpe.com/main_project/api/v1/viewer/customers/US055169452').success(function(response)
			{
				var response  = JSON.parse(response);
				$scope.accountName = response.AccountName;
				//alert ($scope.accountName)
				console.log(response)
				if(response.SummarySupportIncidents)
				{
					 $scope.SupportIncidents = new Array();
					 
					 for(i=0;i<response.SummarySupportIncidents.length;i++)
					 {
						 $scope.SupportIncidents.push(response.SummarySupportIncidents[i])
					 }
					 
					 $rootScope.SupportIncidents = $scope.SupportIncidents; 
				}
				
				if(response.SummarySupportCases)
				{
					 $scope.SupportCases = new Array();
					 
					 for(i=0;i<response.SummarySupportCases.length;i++)
					 {
						 $scope.SupportCases.push(response.SummarySupportCases[i])
					 }
					 
					 $rootScope.SupportCases = $scope.SupportCases; 
				}
				
				if(response.SummaryEnhancements)
				{
					 $scope.SupportEnhancements = new Array();
					 
					 for(i=0;i<response.SummaryEnhancements.length;i++)
					 {
						 $scope.SupportEnhancements.push(response.SummaryEnhancements[i])
					 }
					 
					 $rootScope.SupportEnhancements = $scope.SupportEnhancements; 
				}
				
				if(response.SummaryDefects)
				{
					 $scope.SupportDefects = new Array();
					 
					 for(i=0;i<response.SummaryDefects.length;i++)
					 {
						 $scope.SupportDefects.push(response.SummaryDefects[i])
					 }
					 
					 $rootScope.SupportDefects = $scope.SupportDefects; 
				}
				
				
				if(response.CustomerOrders)
				{
					 $scope.ordersLenth = response.CustomerOrders.length;
					 $scope.CustomerOrders = new Array();
					 
					 for(i=0;i<response.CustomerOrders.length;i++)
					 {
						 if(response.CustomerOrders[i].State != "Fixed")
						 $scope.CustomerOrders.push(response.CustomerOrders[i])
					 }
					 
					 $rootScope.CustomerOrders = $scope.CustomerOrders; 
				}
				 
				// $scope.SupportCases = response.SupportCases;
				// $scope.SupportIncidents = response.SupportIncidents;
				 //$scope.SupportEnhancements = response.SupportEnhancements;
				 //$scope.SupportDefects = response.SupportDefects;
				 $rootScope.CustomerInfo = response;
				 $scope.CustomerInfo = response;
				 $scope.CustomerName = response.AccountName + ' support status'
				 $scope.Contacts  = response.Roster; 
				 
				 if ($scope.Contacts)
						$scope.ContactsLenth = $scope.Contacts.length;
					else
						$scope.ContactsLenth = 0;
					
					
					
					
				 $ionicLoading.hide();
				 $scope.isLoad = false;
				 console.log("response000000000" , response.AccountName)
			})
			
			
			
			
			$scope.getDateFun = function(DT)
			{
				Darray = DT.split(' ')
				return(Darray[4])
			}
			
			$scope.changeTab = function(tab)
			{
				$ionicScrollDelegate.scrollTop();
				$scope.TabSelected = tab;
				if ($scope.TabSelected == 1)
					$scope.CustomerName = $scope.accountName + ' support status'
				if ($scope.TabSelected == 2)
					$scope.CustomerName = $scope.accountName + ' contacts'
				if ($scope.TabSelected == 3)
					$scope.CustomerName = $scope.accountName + ' products'
				
			}
			
			$scope.callTo = function(phone)
			{
				console.log("Phone : " + phone)
				window.location.href = "tel:"+phone
			}
			
			$scope.mailTo = function(mail)
			{
				window.location.href = "mailto:"+mail;
			}
			
			
		//}, 300);
	//});
})


.controller('DetailsCtrl', function($scope, $stateParams,$rootScope,$timeout,$ionicSlideBoxDelegate,$http,$ionicLoading) 
{
	//$scope.$on('$ionicView.enter', function(e) 
	//{
		//$timeout(function() 
		//{
			$scope.isLoading = false;
			
		  	$scope.screenWidth = "345px"
		    $scope.CustumerId = parseInt($stateParams.CustumerId);
			$scope.Type = $stateParams.Type;
			$scope.IndexId = $stateParams.IndexId;
			$scope.Customer = $rootScope.Customers;
			$scope.CustomerName = $rootScope.CustomerInfo.AccountName
			$scope.Len = ''; 
			$scope.CurrentId = parseInt($scope.IndexId)+1;
			$scope.CompleteData = "";
				 
		  $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	
	
			
			
			$scope.$watch('window.innerWidth', function() 
			{
            	$scope.screenWidth  = window.innerWidth + 'px';
				console.log("W : " + $scope.screenWidth)
        	});

		 
		  
		 // console.log("Crs : " , $scope.Customer.CRs);
		  
		  $scope.setDetails = function()
		  {
			  $scope.CurrentId = 1;
			  
			  if ($scope.Type =="Escalation")
			  {
				 // $scope.CompleteData = $rootScope.SupportIncidents;
				//  $scope.SingleData = $rootScope.SupportIncidents[$scope.IndexId];
				 $scope.TypeTitle = 'Escalation';
				 // $scope.Len = $rootScope.SupportIncidents.length;
				 $scope.VerCut = $rootScope.SupportIncidents[$scope.IndexId].Name.split(' ');
				 console.log($scope.VerCut)
				/* $scope.verNum =   $scope.VerCut[2];
				 $scope.verNumPoint = $scope.verNum.split('.')
				 $scope.verNum = $scope.verNumPoint[0]+'_'+$scope.verNumPoint[1];
				 
				 $scope.Ver = $scope.VerCut[0]+' ' + $scope.VerCut[1] + ' ' + $scope.verNum;
				 console.log("VersionInceidens : " ,$scope.Ver  )*/
				 $scope.VerCut = $rootScope.SupportIncidents[$scope.IndexId].Name.replace(".", "_"); 
				 $scope.URL = "https://admast01web.saas.hpe.com/main_project/api/v1/viewer/customers/support/incidents/"+$rootScope.SupportIncidents[$scope.IndexId].AMID+"/product/" + $scope.VerCut ; 
			  }
			  if ($scope.Type =="CRs")
			  {
				  //$scope.CompleteData = $rootScope.SupportDefects;
				//  $scope.SingleData = $rootScope.SupportDefects[$scope.IndexId];
				//  $scope.Len = $rootScope.SupportDefects.length;
				  $scope.TypeTitle = 'CRs';
				  console.log("CR : " + $scope.VerCut)
				  $scope.VerCut = $rootScope.SupportDefects[$scope.IndexId].Name.replace(".", "_"); 
				  $scope.URL = "https://admast01web.saas.hpe.com/main_project/api/v1/viewer/customers/support/defects/"+$rootScope.SupportDefects[$scope.IndexId].AMID+"/product/"+$scope.VerCut ;
			  }
			  if ($scope.Type =="ERs")
			  {
				  $scope.TypeTitle = 'ERs';
				  $scope.VerCut = $rootScope.SupportEnhancements[$scope.IndexId].Name.replace(".", "_"); 
				  console.log("ER : " + $scope.VerCut)
				  $scope.URL = "https://admast01web.saas.hpe.com/main_project/api/v1/viewer/customers/support/enhancements/"+$rootScope.SupportEnhancements[$scope.IndexId].AMID+"/product/"+$scope.VerCut ;
			  }
			  if ($scope.Type =="Support")
			  {
				  $scope.TypeTitle = 'Support';
				  $scope.VerCut = $rootScope.SupportCases[$scope.IndexId].Name.replace(".", "_"); 
				  console.log("SupportCases : " + $scope.VerCut)
				  $scope.navTitle='<p class="HeaderTitle">'+$scope.CustomerName + ' - ' + $scope.TypeTitle+'</p>'
				  $scope.URL = "https://admast01web.saas.hpe.com/main_project/api/v1/viewer/customers/support/cases/"+$rootScope.SupportCases[$scope.IndexId].AMID+"/product/"+$scope.VerCut;
			  }		
			  $scope.getDetails($scope.URL)
			  
		  }

		  
		  $scope.getDetails = function(URL)
		  {
			  $ionicLoading.show({
				  template: 'Loading...'
			})
			
			$scope.isLoading = true;
			  console.log("URL : ", URL)
			  
				$scope.headers = 
				{
							//'Authorization': 'Basic ' + _authdata,
							'Cookie:': $rootScope.cookie,
							'Content-Type': 'application/json; charset=utf-8'               
				}

							
			  $http.get(URL,$scope.headers)
				.success(function (response) 
				{
					///alert (response);
					var response  = JSON.parse(response);
					$scope.isLoading = false
					$ionicLoading.hide();
					
					if ($scope.Type =="Support")
					{
						console.log("Support : ", response.SupportCases)
						if(response.SupportCases)
						{
							$scope.CompleteData = response.SupportCases;
							$scope.SingleData  = response.SupportCases[0];
							$scope.Len = response.SupportCases.length;
		    				$scope.setSelected(0);
						}
					} else
					if ($scope.Type =="ERs")
					{
						console.log("ERs : ", response)
						if(response.SupportEnhancements)
						{
							$scope.CompleteData = response.SupportEnhancements;
							$scope.SingleData  = response.SupportEnhancements[0];
							$scope.Len = response.SupportEnhancements.length;
		    				$scope.setSelected(0);
						}
					}else
					if ($scope.Type =="CRs")
					{
						console.log("CRs : ", response)
						if(response.SupportDefects)
						{
							$scope.CompleteData = response.SupportDefects;
							$scope.SingleData  = response.SupportDefects[0];
							$scope.Len = response.SupportDefects.length;
		    				$scope.setSelected(0);
						}
					}else
					if ($scope.Type =="Escalation")
					{
						console.log("Escalation : ", response)
						if(response.SupportIncidents)
						{
							$scope.CompleteData = response.SupportIncidents;
							$scope.SingleData  = response.SupportIncidents[0];
							$scope.Len = response.SupportIncidents.length;
							$scope.setSelected(0);
						}
					}
					
					;
					
				});
				
				$scope.navTitle='<p class="HeaderTitle">' +$scope.CustomerName + ' 11- ' +$scope.TypeTitle+'</p>'  
		  }
		  
		  $scope.ChangeType = function(type)
		  {
			  $scope.IndexId = parseInt(type);
			  $scope.CurrentId  = $scope.IndexId+1
			 // $scope.setDetails();
			  $scope.setSelected(type);
			  $ionicSlideBoxDelegate.slide(parseInt(type))
		  }
		  
		  $scope.setSelected = function(Selected)
		  {
			  for(var i=0;i<$scope.CompleteData.length;i++)
			  {
				  $scope.CompleteData[i].isSelected = 0;
			  }
			  
			  $scope.CompleteData[Selected].isSelected = 1;
			  $scope.SingleData = $scope.CompleteData[Selected];
		  }
		  
		  $scope.slideHasChanged = function(type)
		  {
			  $scope.IndexId = parseInt(type);
			//  console.log("Type : ", $scope.IndexId);
			  $scope.CurrentId  = $scope.IndexId+1
			//  $scope.setDetails();
			  $scope.setSelected(type);
		  }
		   
		
		  
		  $scope.setDetails();
		 /* $http.post('http://www.tapper.co.il/hp.php').success(function(response)
			{
				$scope.SupportIncidents = new Array();
				 for(i=0;i<response.SupportIncidents.length;i++)
				 {
					 if(response.SupportIncidents[i].State != "Closed")
					 $scope.SupportIncidents.push(response.SupportIncidents[i])
				 }
				 console.log($scope.SupportIncidents)
				 $rootScope.SupportIncidents = $scope.SupportIncidents;
				 
				 $scope.SupportCases = response.SupportCases;
			
				 $scope.SupportEnhancements = response.SupportEnhancements;
				 $scope.SupportDefects = response.SupportDefects;
				 $scope.navTitle='<p class="HeaderTitle">'+response.Area+'</p>'
				 $rootScope.CustomerInfo = response;
				 $scope.Contacts  = response.Roster;
				 //$ionicLoading.hide();
				 $scope.isLoad = false;
			
				 
			
				 
			})*/
			
			
			
			//$scope.ChangeType(0);
		   // $scope.slideHasChanged(0);
		 
		//}, 300);
	//});
})

.filter('cutString_TitlePage', function () {
    return function (value, wordwise, max, tail) 
	{
		value =  value.replace(/(<([^>]+)>)/ig,"");
        if (!value) return '';
		
        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
    //    if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
                value = value.substr(0, lastspace);
            }
     //   }
        return value + (tail || '...');
    };
})


