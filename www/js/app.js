// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','ngCordova'])

.run(function($rootScope,$ionicPlatform,$http,$ionicLoading,$ionicHistory,$state,$cordovaInAppBrowser) {
  $ionicPlatform.ready(function() {
	  
	$rootScope.currState = $state;
	$rootScope.State = '';
	$rootScope.SearchSupplierFlag = 0;
	$rootScope.AndroidbackButton = 0;



		
/*  
function openBrowser() {
   var url = 'https://cordova.apache.org';
   var target = '_blank';
   var options = "location=yes"
   var ref = cordova.InAppBrowser.open(url, target, options);


}
*/

	$rootScope.$watch('currState.current.name', function(newValue, oldValue) {
      $rootScope.State = newValue;
    });  
	


	
	

function htmlToPlaintext(text) {
  return text ? String(text).replace(/<[^>]+>/gm, '') : '';
}
	
	$rootScope.launchBrowser = function()
	{
		

		$rootScope.cookie = '';
		iabRef = window.open("https://admast01web.saas.hpe.com/main_project/api/v1/startup?TENANTID=507989718", '_blank', 'location=yes');

	   iabRef.addEventListener('loadstart', loadstartCallback);
	   iabRef.addEventListener('loadstop', loadstopCallback);
	   iabRef.addEventListener('loadloaderror', loaderrorCallback);
	   iabRef.addEventListener('exit', exitCallback);

	   function loadstartCallback(event) {
		   //alert (event.url);
		  console.log('Loading started: '  + event.url)
	   }

	   function loadstopCallback(event) {
		   //alert (event.url);
		   
		iabRef.executeScript(
			{ code: "document.body.innerHTML" },
			function( values ) {
				

				var plain_text = htmlToPlaintext( values[ 0 ] );
				var json  = JSON.parse(eval(plain_text));
				//alert (json.text)
				//alert (json.LWSSO_COOKIE_KEY)
				if (json.LWSSO_COOKIE_KEY)
				{
					$rootScope.cookie = json.LWSSO_COOKIE_KEY;
					iabRef.close();
					
					$rootScope.loadCustomers();

		


		
				}

			}
		);

			
			 console.log('Loading finished: ' + event.url)
		   }

		   function loaderrorCallback(error) {
			  console.log('Loading error: ' + error.message)
		   }

		   function exitCallback() {
			   //alert (111);
			  console.log('Browser is closed...')
		   }
			
	}
	
	$rootScope.launchBrowser();
	

	$rootScope.loadCustomers = function()
	{
		$rootScope.SearchSupplierFlag = 0;
		
		$ionicLoading.show({
			  template: 'Loading customers...',
			  //noBackdrop : true,
			  duration : 10000				  
		})

		
		var headers = {
					//'Authorization': 'Basic ' + _authdata,
					'Cookie:': $rootScope.cookie,
					'Content-Type': 'application/json; charset=utf-8'               
				}

		
		$http.get('https://admast01web.saas.hpe.com/main_project/api/v1/viewer/customers/',headers )
		.success(function(data, status, headers, config) 
		{
			//alert (data);
			var newdata  = JSON.parse(data);
			//alert (data);
			//alert(JSON.stringify(newdata));
			
		  $rootScope.Customers = newdata;	
		  $ionicLoading.hide();
		 // console.log("Customers : " , $rootScope.Customers )



		}).error(function(data, status,headers,config) 
		{
			$ionicLoading.hide();
			//alert ("bad");
			//alert ("bad")
		});		
	}
	
	

	$ionicPlatform.registerBackButtonAction(function (event) 
	{
		if($rootScope.State == 'app.playlists') 
		{
			
			
			if ($rootScope.SearchSupplierFlag == 1)
			{
				
				$rootScope.AndroidbackButton = 1;
				$rootScope.loadCustomers();
			}
			else
			{
				 confirmbox = confirm("would you like to close the app?");
				 if (confirmbox)
					navigator.app.exitApp();
				 else
					event.preventDefault();				
			}

		}
		//else
		//{
		//	 $ionicHistory.goBack();
		//}
	},100);

					

	
   
	/*$http({
	  url: 'https://admast01web.saas.hpe.com/main_project/api/v1/startup?TENANTID=507989718',
	  method: 'GET',
	  params: {},
	  paramSerializer: '$httpParamSerializerJQLike'
	})*/
	
/*	$http({
		url: 'https://admast01web.saas.hpe.com/main_project/api/v1/status',
		method: 'POST',
		data: $httpParamSerializerJQLike({'TENANTID':'507989718'}),
		headers: {
		  'Content-Type': 'application/x-www-form-urlencoded'
		}
  	})*/
		 
		

	/*
	  $http.jsonp('https://admast01web.saas.hpe.com/main_project/api/v1/startup?TENANTID=507989718&callback=JSON_CALLBACK').success(function (data) 
	  {
		  console.log(data)
	  }).error(function(data, status,headers,config) 
    {
        console.log("Err : " , data ," :: ", status , " :: " , headers ," :: ", config)
    });;
	*/
		
		
/*	$http({
	  url: 'https://admast01web.saas.hpe.com/main_project/api/v1/status?TENANTID=507989718',
	  method: 'GET',
	  params: {},
	  paramSerializer: '$httpParamSerializerJQLike'
	})
	.success(function (response) 
	{
		console.log("S2")
          $rootScope.Customers = response;	
    	  $ionicLoading.hide();
		 // console.log("Customers : " , $rootScope.Customers )
    });*/

	
	/*$http({
		url: myUrl,
		method: 'POST',
		data: $httpParamSerializerJQLike(myData),
		headers: {
		  'Content-Type': 'application/x-www-form-urlencoded'
		}
  	});
  
  
	console.log("S1")
	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	
	$http.post('https://admast01web.saas.hpe.com/main_project/api/v1/startup?TENANTID=507989718')
    .success(function (response) 
	{
		console.log("S2")
          $rootScope.Customers = response;	
    	  $ionicLoading.hide();
		 // console.log("Customers : " , $rootScope.Customers )
    });*/
	

/*	iabRef.addEventListener('exit', iabClose);
	iabRef.addEventListener('loadstart', iabLoadStart);
	iabRef.addEventListener('loadstop', iabLoadStop);

		 
	 function iabClose(event) 
	{
		 alert(event.type + ' - ' + event.url);
         //iabRef.removeEventListener('exit', iabClose);
    }
	
    function iabLoadStart(event) {
        alert(event.type + ' - ' + event.url);
    }

    function iabLoadStop(event) 
	{
		 alert(event.type + ' - ' + event.url);
	}*/
	
//	$rootScope.Customers = new Array($rootScope.CustomersName);
	
	
	
	

  	
	
	console.log("ININ")
	//https://admast01web.saas.hpe.com/main_project/api/status/?TENANTID=507989718

//https://admast01web.saas.hpe.com/main_project/api/customers/?TENANTID=507989718

/*
	$http.get('https://admast01web.saas.hpe.com/main_project/api/v1/viewer/customers?TENANTID=507989718')
    .success(function(data, status, headers, config) 
	{
		 console.log("admast01web : "  )
		 alert(response)
    }).error(function(data, status,headers,config) 
    {
        console.log("Err : " , data ," :: ", status , " :: " , headers ," :: ", config)
    });*/
	
	//iabRef = window.open("https://admast01web.saas.hpe.com/main_project/api/v1/viewer/customers?TENANTID=507989718", '_blank', 'location=yes','toolbar=no');
	
	
	/*$http.get('http://54.164.168.168:8080/main_project/api/v1/viewer/customers')
    .success(function (response) 
	{
          $rootScope.Customers = response;	
    	  $ionicLoading.hide();
		 // console.log("Customers : " , $rootScope.Customers )
    });*/
	
	
			
	/*$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	console.log("sss")
	
	$http.post('http://www.tapper.co.il/hp/customers.php').success(function(response)
	{
	    $rootScope.Customers = response;	
	});
	
	$http.post('http://www.tapper.co.il/hp/customers1.php').success(function(response)
	{
	    $rootScope.Customers1 = response;
		console.log($rootScope.Customers1)		
	});*/
			
	/*$http.get('http://www.tapper.co.il/hp/customers.json').success(function(response) 
	{	
		$rootScope.Customers = response;
		console.log($rootScope.Customers)
	});*/
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
	
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
	console.log("ININ1")
  });
})
/*
.config(['$httpProvider', function($httpProvider) {
        $httpProvider.defaults.useXDomain = true;
        $httpProvider.defaults.headers.common = 'Content-Type: application/json';
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }
])
*/


.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {
	$ionicConfigProvider.backButton.previousTitleText(false).text('');
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html'
      }
    }
  })

  .state('app.browse', {
      url: '/browse',
      views: {
        'menuContent': {
          templateUrl: 'templates/browse.html'
        }
      }
    })
    .state('app.playlists', {
      url: '/playlists',
      views: {
        'menuContent': {
          templateUrl: 'templates/playlists.html',
          controller: 'PlaylistsCtrl'
        }
      }
    })

  .state('app.single', {
    url: '/single/:playlistId',
    views: {
      'menuContent': {
        templateUrl: 'templates/single.html',
        controller: 'SingleCtrl'
      }
    }
  })
  
  
  .state('app.details', {
    url: '/details/:CustumerId/:Type/:IndexId',
    views: {
      'menuContent': {
        templateUrl: 'templates/details.html',
        controller: 'DetailsCtrl'
      }
    }
  })

  
  
  ;
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/playlists');
});
