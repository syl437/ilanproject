// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers'])

.run(function($rootScope,$ionicPlatform,$http,$ionicLoading) {
  $ionicPlatform.ready(function() {
	
	$rootScope.Customers = "";
	$rootScope.CustomerInfo = "";
	$rootScope.Host = "http://54.164.168.168:8080"
	$rootScope.CustomersName = 
	{
		"name":"AT&T",
		"area":"AMS-East",
		"img":"img/Icons/i1.png",
		"id":"1"
	}
	
//	$rootScope.CustomersNames = new Array($rootScope.CustomersName);
	
	
	
	
	$ionicLoading.show({
				  template: 'Loading...'
	})
  			
	$http.get('http://54.164.168.168:8080/main_project/api/v1/viewer/customers')
    .success(function (response) 
	{
          $rootScope.CustomersNames = response;	
    	  $ionicLoading.hide();
		  console.log("Customers : " , $rootScope.CustomersNames )
    });
			
	/*$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	console.log("sss")
	
	$http.post('http://www.tapper.co.il/hp/customers.php').success(function(response)
	{
	    $rootScope.Customers = response;	
	});
	
	$http.post('http://www.tapper.co.il/hp/customers1.php').success(function(response)
	{
	    $rootScope.Customers1 = response;
		console.log($rootScope.Customers1)		
	});*/
			
	/*$http.get('http://www.tapper.co.il/hp/customers.json').success(function(response) 
	{	
		$rootScope.Customers = response;
		console.log($rootScope.Customers)
	});*/
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
	
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {
	$ionicConfigProvider.backButton.previousTitleText(false).text('');
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html'
      }
    }
  })

  .state('app.browse', {
      url: '/browse',
      views: {
        'menuContent': {
          templateUrl: 'templates/browse.html'
        }
      }
    })
    .state('app.playlists', {
      url: '/playlists',
      views: {
        'menuContent': {
          templateUrl: 'templates/playlists.html',
          controller: 'PlaylistsCtrl'
        }
      }
    })

  .state('app.single', {
    url: '/single/:playlistId',
    views: {
      'menuContent': {
        templateUrl: 'templates/single.html',
        controller: 'SingleCtrl'
      }
    }
  })
  
  
  .state('app.details', {
    url: '/details/:CustumerId/:Type/:IndexId',
    views: {
      'menuContent': {
        templateUrl: 'templates/details.html',
        controller: 'DetailsCtrl'
      }
    }
  })

  
  
  ;
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/playlists');
});
