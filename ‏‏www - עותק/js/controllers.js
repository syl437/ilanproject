angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout , $rootScope) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

})

.controller('PlaylistsCtrl', function($scope,$http,$rootScope,$timeout,$ionicScrollDelegate) 
{
	$scope.$on('$ionicView.enter', function(e) 
	{
		//$timeout(function() 
		//{
			
		//	$scope.CustomersNames = $rootScope.CustomersNames;
			
			$scope.Host = $rootScope.Host				
			$rootScope.$watch('Customers', function() 
			{
    		    $scope.Customers = $rootScope.Customers;
				/*$scope.CustomersNames = [{
									"AMID": "IL600026090",
									"AccountName": ".CROW TECHNOLOGIES 1977 LTD ",
									"Region": "EMEA",
									"Subregion": "CEE",
									"AccountType": "images/customers/ABB.png",
									"Roster": [],
									"SupportCases": [],
									"SupportIncidents": [],
									"CustomerOrders": []
								}]*/
								console.log($scope.CustomersNames)
			});
			
			$scope.blurSearch = function()
			{
				$ionicScrollDelegate.scrollTop();
			}
		
		//}, 0);
	});
})

.controller('SingleCtrl', function($scope, $stateParams,$rootScope,$timeout,$http,$ionicLoading) 
{
	//$scope.$on('$ionicView.enter', function(e) 
	//{
		//$timeout(function() 
		//{
			$scope.Id = $stateParams.playlistId;
			$scope.Customer = $rootScope.Customers;
			$scope.CustomerName = $scope.Customer.AccountName
			$scope.Contacts = "";
			$scope.SupportCases = [];
			$scope.isLoad = true;
			$scope.navTitle='';
			//alert ($scope.Id);
			"<p class='HeaderTitle'>ADM Customers</p>"
			$scope.TabSelected = 1;
			
			$ionicLoading.show({
				  template: 'Loading...'
			})
			console.log("Start")
			//$http.get('http://54.164.168.168:8080/main_project/api/v1/viewer/customers/US055169452')
			/*$http.get('http://www.tapper.co.il/hp.php')
			.success(function (response) 
			{
				console.log("Load")
				 $scope.SupportCases = response.SupportCases;
				 $scope.SupportIncidents = response.SupportIncidents;
				 $ionicLoading.hide();
				 $scope.isLoad = false;
			});*/
			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	
	
			$http.get('http://54.164.168.168:8080/main_project/api/v1/viewer/customers/US055169452').success(function(response)
			{
				 $scope.SupportIncidents = new Array();
				 
				 for(i=0;i<response.SupportIncidents.length;i++)
				 {
					 if(response.SupportIncidents[i].State != "Closed")
					 $scope.SupportIncidents.push(response.SupportIncidents[i])
				 }
				 
				 $rootScope.SupportIncidents = $scope.SupportIncidents; 
				 
				 $scope.SupportCases = new Array();
				 
				 for(i=0;i<response.SupportCases.length;i++)
				 {
					 if(response.SupportCases[i].Status != "Closed")
					 $scope.SupportCases.push(response.SupportCases[i])
				 }
				 
				 $rootScope.SupportCases = $scope.SupportCases; 
				 
				 $scope.SupportEnhancements = new Array();
				 
				 for(i=0;i<response.SupportEnhancements.length;i++)
				 {
					 if(response.SupportEnhancements[i].Status != "Closed")
					 $scope.SupportEnhancements.push(response.SupportEnhancements[i])
				 }
				 
				 $rootScope.SupportEnhancements = $scope.SupportEnhancements; 
				 
				 $scope.SupportDefects = new Array();
				 
				 for(i=0;i<response.SupportDefects.length;i++)
				 {
					 if(response.SupportDefects[i].State != "Fixed")
					 $scope.SupportDefects.push(response.SupportDefects[i])
				 }
				 
				 $rootScope.SupportDefects = $scope.SupportDefects; 
				 
				 
				// $scope.SupportCases = response.SupportCases;
				// $scope.SupportIncidents = response.SupportIncidents;
				 //$scope.SupportEnhancements = response.SupportEnhancements;
				 //$scope.SupportDefects = response.SupportDefects;
				 $scope.navTitle='<p class="HeaderTitle">'+response.Area+'</p>'
				 $rootScope.CustomerInfo = response;
				 $scope.Contacts  = response.Roster;
				 $ionicLoading.hide();
				 $scope.isLoad = false;
				 console.log("response" , response)
			})
			
			
			$scope.changeTab = function(tab)
			{
				$scope.TabSelected = tab
			}
			
			$scope.callTo = function(phone)
			{
				console.log("Phone : " + phone)
				window.location.href = "tel:"+phone
			}
			
			$scope.mailTo = function(mail)
			{
				window.location.href = "mailto:"+mail;
			}
			
			
		//}, 300);
	//});
})


.controller('DetailsCtrl', function($scope, $stateParams,$rootScope,$timeout,$ionicSlideBoxDelegate,$http) 
{
	//$scope.$on('$ionicView.enter', function(e) 
	//{
		//$timeout(function() 
		//{
		  
		    $scope.CustumerId = parseInt($stateParams.CustumerId);
			$scope.Type = $stateParams.Type;
			$scope.IndexId = $stateParams.IndexId;
			$scope.Customer = $rootScope.Customers;
			$scope.CustomerName = $rootScope.CustomerInfo.Area
			$scope.Len = ''; 
			$scope.CurrentId = parseInt($scope.IndexId)+1;
			
				
				 
		  $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	
	
			
			
			

		 
		  
		 // console.log("Crs : " , $scope.Customer.CRs);
		  
		  $scope.setDetails = function()
		  {
			  if ($scope.Type =="Escalation")
			  {
				  $scope.CompleteData = $rootScope.SupportIncidents;
				  $scope.SingleData = $rootScope.SupportIncidents[$scope.IndexId];
				  $scope.TypeTitle = 'Escalation';
				  $scope.navTitle='<p class="HeaderTitle">'+$scope.CustomerName + ' - ' + $scope.TypeTitle+'</p>'
				  $scope.Len = $rootScope.SupportIncidents.length;
			  }
			  if ($scope.Type =="CRs")
			  {
				  $scope.CompleteData = $rootScope.SupportDefects;
				  $scope.SingleData = $rootScope.SupportDefects[$scope.IndexId];
				  $scope.TypeTitle = 'CRs';
				  $scope.Len = $rootScope.SupportDefects.length;
			  }
			  if ($scope.Type =="ERs")
			  {
				  $scope.CompleteData = $rootScope.SupportEnhancements;
				  $scope.SingleData = $rootScope.SupportEnhancements[$scope.IndexId];
				  $scope.TypeTitle = 'ERs';
				  $scope.Len = $rootScope.SupportEnhancements.length;
			  }
			  if ($scope.Type =="Support")
			  {
				  $scope.CompleteData = $rootScope.SupportCases;
				  $scope.SingleData = $rootScope.SupportCases[$scope.IndexId];
				  $scope.TypeTitle = 'Support';
				  $scope.navTitle='<p class="HeaderTitle">'+$scope.CustomerName + ' - ' + $scope.TypeTitle+'</p>'
				  $scope.Len = $rootScope.SupportCases.length;
			  }		  
		  }

		  
		  $scope.ChangeType = function(type)
		  {
			  $scope.IndexId = parseInt(type);
			 // console.log("Type : ", $scope.IndexId);
			  $scope.CurrentId  = $scope.IndexId+1
			  $scope.setDetails();
			  $scope.setSelected(type);
			//  alert(type)
			  $ionicSlideBoxDelegate.slide(parseInt(type))
		  }
		  
		  $scope.setSelected = function(Selected)
		  {
			  for(var i=0;i<$scope.CompleteData.length;i++)
			  {
				  $scope.CompleteData[i].isSelected = 0;
			  }
			  
			  $scope.CompleteData[Selected].isSelected = 1;
			  
		  }
		  
		  $scope.slideHasChanged = function(type)
		  {
			  $scope.IndexId = parseInt(type);
			//  console.log("Type : ", $scope.IndexId);
			  $scope.CurrentId  = $scope.IndexId+1
			  $scope.setDetails();
			  $scope.setSelected(type);
		  }
		   
		
		  
		  
		 /* $http.post('http://www.tapper.co.il/hp.php').success(function(response)
			{
				$scope.SupportIncidents = new Array();
				 for(i=0;i<response.SupportIncidents.length;i++)
				 {
					 if(response.SupportIncidents[i].State != "Closed")
					 $scope.SupportIncidents.push(response.SupportIncidents[i])
				 }
				 console.log($scope.SupportIncidents)
				 $rootScope.SupportIncidents = $scope.SupportIncidents;
				 
				 $scope.SupportCases = response.SupportCases;
			
				 $scope.SupportEnhancements = response.SupportEnhancements;
				 $scope.SupportDefects = response.SupportDefects;
				 $scope.navTitle='<p class="HeaderTitle">'+response.Area+'</p>'
				 $rootScope.CustomerInfo = response;
				 $scope.Contacts  = response.Roster;
				 //$ionicLoading.hide();
				 $scope.isLoad = false;
			
				 
			
				 
			})*/
			
			
			$scope.setDetails();
			//$scope.ChangeType(0);
		   // $scope.slideHasChanged(0);
		    $scope.setSelected($scope.IndexId);
		//}, 300);
	//});
});



